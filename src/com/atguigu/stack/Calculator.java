package com.atguigu.stack;

public class Calculator {

	public static void main(String[] args){
		String experession = "300+2*6-2";
		//创建两个栈，数栈和运算符栈
		ArrayStack1 numStack = new ArrayStack1(10);
		ArrayStack1 operStack = new ArrayStack1(10);
		//定义所需变量
		int index= 0;
		int num1=0;
		int num2=0;
		int oper=0;
		int result=0;
		char ch=' ';
		String strNum = "";  //拼接多位数
		while (true) {
			//得到表达式中每一个字符
			ch=experession.substring(index,index+1).charAt(0);
			if(operStack.isOper(ch)){  //如果是运算符
				//如果运算符栈为null，直接入栈
				if (operStack.isNull()) {
					operStack.push(ch);
				}else {
					//判断优先级
					//优先级比栈顶元素小，放栈底
					if(operStack.priority(ch)<=operStack.priority(operStack.showTop())){
						num1=numStack.pop();
						num2=numStack.pop();
						oper=operStack.pop();
						result=numStack.cal(num1,num2,oper);
						//运算结果入数栈
						numStack.push(result);
						//将运算符入运算符栈
						operStack.push(ch);
					}else {
						operStack.push(ch);
					}
				}
			}else {   //是数字
				//注意：多位数
				strNum+=ch;
				//如果ch已经是表达式最后一位，直接入栈
				if (index == experession.length() - 1) {
					numStack.push(Integer.parseInt(strNum));
				}else {
					//下一位是运算符，拼接后直接入栈
					if(operStack.isOper(experession.substring(index+1,index+2).charAt(0))){
						numStack.push(Integer.parseInt(strNum));
						strNum="";
					}
				}
			}
			index++;
			if(index>=experession.length()){
				break;
			}
		}
		//表达式扫描完毕，将栈中的数进行最后一次运算
		while (true) {
			if (operStack.isNull()) {
				break;
			}
			num1=numStack.pop();
			num2=numStack.pop();
			oper=operStack.pop();
			result=numStack.cal(num1,num2,oper);
			numStack.push(result);
		}
		System.out.printf("%s=%d",experession,numStack.pop());
	}
}
class ArrayStack1{
	public int maxSize;
	public int[] stack;
	public int top=-1;  //栈顶

	public ArrayStack1(int maxSize){
		this.maxSize=maxSize;
		stack=new int[this.maxSize];
	}

	public boolean isNull(){
		return top==-1;
	}

	public boolean isFull(){
		return top==maxSize-1;
	}

	//入栈
	public void push(int num){
		if(isFull()){
			System.out.println("栈满");
			return;
		}
		top++;
		stack[top]=num;
	}

	//出栈
	public int pop(){
		if(isNull()){
			throw new RuntimeException("栈已为空!");
		}
		return stack[top--];
	}

	//展示栈
	public void show(){
		if(isNull()){
			System.out.println("栈已为空!");
			return;
		}
		for (int i=top; i>=0; i--) {
			System.out.printf("stack[%d]=%d\n",i,stack[i]);
		}
	}

	//查看栈顶val
	public int showTop(){
		return stack[top];
	}

	//判断运算符的优先级
	public int priority(int oper){
		if(oper == '*'||oper == '/'){
			return 1;
		}else if(oper == '+'||oper == '-'){
			return 0;
		}else {
			return -1;
		}
	}

	//判断是否是运算符
	public boolean isOper(char val){
		return val=='+'||val=='-'||val=='*'||val=='/';
	}

	//计算方法
	public int cal(int num1,int num2,int oper){
		int result=0;
		switch (oper){
			case '+':
				result=num1+num2;
				break;
			case '-':
				result=num2-num1;
				break;
			case '*':
				result=num1*num2;
				break;
			case '/':
				result=num2/num1;
				break;
		}
		return result;
	}
}