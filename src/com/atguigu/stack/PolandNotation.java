package com.atguigu.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 逆波兰计算器(后缀表达式)
 */
public class PolandNotation {

	public static void main(String[] args){
		//定义一个中缀表达式
		// 1+((2+3)*4)-5
		String inffixExpression = "1+((2+3)*4)-5";
		List<String> infixExpressionList = toInfixExpressionList(inffixExpression);
		List<String> suffixExpressionList = parseSuffixExpressionList(infixExpressionList);
		System.out.println(suffixExpressionList);

//		//定义个逆波兰表达式
//		//(30+4)*5-6  =>  3 4 + 5 * 6 -     =>164
//		String suffixExpression = "30 4 + 5 * 6 -";
//		List<String> suffixExpressionList = getListString(suffixExpression);
		int result = calculate(suffixExpressionList);
		System.out.println(result);
	}

	/**
	 * 将中缀表达式转成后缀表达式
	 * @param infixExpressionList
	 * @return
	 */
	private static List<String> parseSuffixExpressionList(List<String> infixExpressionList){
		//定义一个栈
		Stack<String> s1 = new Stack<>();
		//因为s2全程没有进行pop，而且最后还需要逆序，所以用list代替
		List<String> s2 = new ArrayList<>();
		for (String item : infixExpressionList) {
			//如果是一个数，直接加入s2
			if (item.matches("\\d+")) {
				s2.add(item);
			} else if (item.equals("(")) {
				s1.push(item);
			} else if (item.equals(")")) {
				//如果是 ) .依次弹出s1，直到栈顶元素是(
				while (!s1.peek().equals("(")){
					s2.add(s1.pop());
				}
				//弹出( ,使小括号消除
				s1.pop();
			}else {
				//当item的优先级小于等于s1栈顶运算符，将s1栈顶运算符弹出并加入s2，再次转到4.1 与s1中新的栈顶运算符比较
				while (s1.size()!=0 && Operation.getValue(s1.peek())>=Operation.getValue(item)){
					s2.add(s1.pop());
				}
				//还需将item入栈
				s1.push(item);
			}
		}
		//将s1中剩余的运算符依次弹出并加入到s2
		while (s1.size() != 0) {
			s2.add(s1.pop());
		}
		return s2;      //因为存放的是list，所以不需要逆序
	}

	/**
	 * 将中缀表达式String转List
	 * @param inffixExpression
	 * @return
	 */
	private static List<String> toInfixExpressionList(String inffixExpression){
		char[] chars = inffixExpression.toCharArray();
		String str = "";
		List<String> list = new ArrayList<>();
		for (int i=0;i<chars.length;i++) {
			//如果是数字，考虑多位数
			if(String.valueOf(chars[i]).matches("\\d+")){
				str+=chars[i];
				//如果是最后一位数字，直接add
				if(i==chars.length-1){
					list.add(str);
				}
			}else {
				if(!str.isEmpty()){
					list.add(str);
				}
				list.add(String.valueOf(chars[i]));
				str = "";
			}
		}
		return list;
	}



	/**
	 * 将表达式转换成list
	 * @param suffixExpression
	 * @return
	 */
	private static List<String> getListString(String suffixExpression) {
		String[] split = suffixExpression.split(" ");
		List<String> list = new ArrayList<>();
		for (String s : split) {
			list.add(s);
		}
		return list;
	}

	/**
	 * 计算
	 * @param list
	 * @return
	 */
	private static int calculate(List<String> list) {
		//创建一个栈
		Stack<String> stack = new Stack<>();
		for (String s : list) {
			//使用正则取数
			//匹配的是多位数
			if(s.matches("\\d+")){
				//入栈
				stack.push(s);
			}else {
				int num2= Integer.parseInt(stack.pop());
				int num1= Integer.parseInt(stack.pop());
				int result=0;
				switch(s){
					case "+":
						result=num1+num2;
						break;
					case "-":
						result=num1-num2;
						break;
					case "*":
						result=num1*num2;
						break;
					case "/":
						result=num1/num2;
						break;
				}
				//将结果入栈
				stack.push(String.valueOf(result));
			}
		}
		return Integer.parseInt(stack.pop());
	}
	
}

//该类返回运算符优先级
class Operation{
	private static final int ADD=1;
	private static final int SUB=1;
	private static final int MUL=2;
	private static final int DIV=2;

	//返回对应优先级
	public  static int getValue(String s){
		int result= 0;
		switch(s){
			case "+":
				result=ADD;
				break;
			case "-":
				result=SUB;
				break;
			case "*":
				result=MUL;
				break;
			case "/":
				result=DIV;
				break;
		}
		return result;
	}
}