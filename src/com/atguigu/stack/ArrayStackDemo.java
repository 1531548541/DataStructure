package com.atguigu.stack;

/**
 * 模拟栈
 */
public class ArrayStackDemo {
	public static void main(String[] args){
		ArrayStack arrayStack = new ArrayStack(3);
		arrayStack.push(1);
		arrayStack.push(2);
		arrayStack.show();
		arrayStack.pop();
		arrayStack.pop();
		arrayStack.push(2);
		arrayStack.show();
	}
}

//创建一个栈对象
class ArrayStack{
	public int maxSize;
	public int[] stack;
	public int top=-1;  //栈顶

	public ArrayStack(int maxSize){
		this.maxSize=maxSize;
		stack=new int[this.maxSize];
	}

	public boolean isNull(){
		return top==-1;
	}

	public boolean isFull(){
		return top==maxSize-1;
	}

	//入栈
	public void push(int num){
		if(isFull()){
			System.out.println("栈满");
			return;
		}
		top++;
		stack[top]=num;
	}

	//出栈
	public void pop(){
		if(isNull()){
			System.out.println("栈已为空!");
			return;
		}
		System.out.printf("%d已出栈\n",stack[top]);
		top--;
	}

	//展示栈
	public void show(){
		if(isNull()){
			System.out.println("栈已为空!");
			return;
		}
		for (int i=top; i>=0; i--) {
			System.out.printf("stack[%d]=%d\n",i,stack[i]);
		}
	}
}
