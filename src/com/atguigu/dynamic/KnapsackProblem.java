package com.atguigu.dynamic;

/**
 * 背包问题
 */
public class KnapsackProblem {
    public static void main(String[] args) {
        int[] w={1,4,3};
        int[] val={1500,3000,2000};
        int m=4;  //背包的容量
        int n=val.length;  //物品的长度

        //创建二维数组
        //v[i][j]表示在前i个物品中能够装入容量为j的背包中的最大价值
        int[][] v=new int[n+1][m+1];
        int[][] path=new int[n+1][m+1];
        
        //根据公式动态规划处理
        for (int i = 1; i < v.length; i++) {
            for (int j = 1; j < v[0].length; j++) {
                //公式
                if(w[i-1]>j){
                    v[i][j]=v[i-1][j];
                }else {
                    //因为我们i是从1开始的，所以公式要调整
//                    v[i][j]=Math.max(v[i-1][j],val[i-1]+v[i-1][j-w[i-1]]);
                    //为了记录商品存放到背包的情况,我们不能简单地使用上面的公式
                    if(v[i-1][j]<val[i-1]+v[i-1][j-w[i-1]]){
                        v[i][j]=val[i-1]+v[i-1][j-w[i-1]];
                        //把当前的情况记录到path
                        path[i][j]=1;
                    }else{
                        v[i][j]=v[i-1][j];
                    }

                }
            }
        }

        //输出
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                System.out.print(v[i][j]+" ");
            }
            System.out.println();
        }

        int i=path.length-1;
        int j=path[0].length-1;
        while(i>0&&j>0){  //从path的最后开始找
            if(path[i][j]==1){
                System.out.printf("第%d个商品放入到背包\n",i);
                j-=w[i-1];
            }
            i--;
        }
    }
}
