package com.atguigu.hash;

import java.util.HashMap;
import java.util.Map;

/**
 * 参考hashMap源码
 *
 * map负责查找，构建一个虚拟的双向链表，Node是数据载体
 *
 * 左边是最新  用addHead
 * 右边是最新  用addTail
 */
public class LRU2 {

    private int capacity;
    Map<Integer,Node<Integer,Integer>> map;
    DoubleLinkedList<Integer,Integer> doubleLinkedList;

    public LRU2(int capacity){
        this.capacity=capacity;
        map=new HashMap<>();
        doubleLinkedList=new DoubleLinkedList<>();
    }

    public int get(int key){
        if(!map.containsKey(key)){
            return -1;
        }
        Node<Integer, Integer> node = map.get(key);
        doubleLinkedList.removeNode(node);
//        doubleLinkedList.addHead(node);
        doubleLinkedList.addTail(node);
        return node.key;
    }

    //saveOrUpdate
    public void put(int key,int value){
        if(map.containsKey(key)){  //update
            Node<Integer, Integer> node = map.get(key);
            node.value=value;
            map.put(key,node);
            doubleLinkedList.removeNode(node);
//            doubleLinkedList.addHead(node);
            doubleLinkedList.addTail(node);
        }else {  //add
            if(map.size()==capacity){  //满了，先删
//                Node<Integer,Integer> node1 = doubleLinkedList.getLast();
                Node<Integer,Integer> node1 = doubleLinkedList.getFirst();
                map.remove(node1.key);
                doubleLinkedList.removeNode(node1);
            }
            Node<Integer,Integer> newNode=new Node<>(key,value);
            map.put(key,newNode);
//            doubleLinkedList.addHead(newNode);
            doubleLinkedList.addTail(newNode);
        }
    }

    public void printDoubleLinkedList(){
        Node temp=doubleLinkedList.head.next;
        while(temp!=null&&temp.key!=null){
            System.out.print(temp.key+" ");
            temp=temp.next;
        }
        System.out.println();
    }


    public static void main(String[] args) {
        LRU2 lru2=new LRU2(3);
        lru2.put(1,1);
        lru2.put(2,2);
        lru2.put(3,3);
        lru2.printDoubleLinkedList();
        lru2.get(3);
//        lru2.put(4,4);
//        lru2.put(3,3);
        lru2.printDoubleLinkedList();
    }



    class Node<K,V>{
        K key;
        K value;
        Node<K,V> pre;
        Node<K,V> next;

        public Node(){
            this.pre=this.next=null;
        }

        public Node(K key,K value){
            this.key=key;
            this.value=value;
            this.pre=this.next=null;
        }
    }

    class DoubleLinkedList<K,V>{
        Node<K,V> head;
        Node<K,V> tail;

        public DoubleLinkedList(){
            head=new Node<>();
            tail=new Node<>();
            head.next=tail;
            tail.pre=head;
        }

        public void addHead(Node<K,V> node){
            node.next=head.next;
            node.pre=head;
            head.next.pre=node;
            head.next=node;
        }

        public void addTail(Node<K,V> node){
            node.next=tail;
            node.pre=tail.pre;
            tail.pre.next=node;
            tail.pre=node;
        }

        public void removeNode(Node<K,V> node){
            node.next.pre=node.pre;
            node.pre.next=node.next;
            node.pre=null;
            node.next=null;
        }

        public Node getFirst(){
            return head.next;
        }

        public Node getLast(){
            return tail.pre;
        }
    }

}
