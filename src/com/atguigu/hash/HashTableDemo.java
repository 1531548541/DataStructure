package com.atguigu.hash;

import java.util.Scanner;

/**
 * 哈希表
 */
public class HashTableDemo {
	public static void main(String[] args) {
		HashTable hashTable = new HashTable(7);

		//写个简单的菜单
		String key="";
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("add");
			System.out.println("list");
			System.out.println("find");
			System.out.println("exit");
			key=scanner.next();
			switch(key){
			    case "add":
			    	System.out.println("输入id");
			    	int id=scanner.nextInt();
			    	System.out.println("输入name");
			    	String name=scanner.next();
			    	hashTable.add(new Emp(id,name));
			        break;
				case "list":
					hashTable.list();
					break;
				case "find":
					System.out.println("请输入需要查找的id");
					id= scanner.nextInt();
					hashTable.findById(id);
					break;
				case "exit":
					scanner.close();
					System.exit(0);
			    default:
			        break;
			}
		}
	}
}

//hashTable
class HashTable {
	public EmpLinkedList[] empLinkedListArray;
	public int size=7;  //表示链表数量

	public HashTable(int size) {
		//初始化
		empLinkedListArray = new EmpLinkedList[size];
		for (int i = 0; i < size; i++) {
			empLinkedListArray[i]=new EmpLinkedList();
		}
	}

	//添加雇员
	public void add(Emp emp) {
		//确定该员工应该加入到哪个链表
		int empLinkedListNo=hashFun(emp.id);
		//添加
		empLinkedListArray[empLinkedListNo].add(emp);
	}

	//根据id查找员工
	public void findById(int id){
		//确定该员工应该加入到哪个链表
		int empLinkedListNo=hashFun(id);
		Emp emp = empLinkedListArray[empLinkedListNo].findById(id);
		if (emp != null) {
			System.out.println("在第"+empLinkedListNo+"个链表查找到该员工id为"+id);
		}else {
			System.out.println("在哈希表中没有该雇员信息!");
		}
	}

	//展示所有链表
	public void list(){
		for (int i = 0; i < size; i++) {
			empLinkedListArray[i].list(i);
		}
	}

	//散列函数(使用取模法)
	public int hashFun(int id) {
		return id % size;
	}
}

class Emp {
	public int id;
	public String name;
	public Emp next;  //下一个节点

	public Emp(int id, String name) {
		this.id = id;
		this.name = name;
	}
}

//链表
class EmpLinkedList {
	public Emp head;  //头指针


	//在最后一个节点后添加员工
	public void add(Emp emp) {
		if (head == null) {  //如果链表为null，直接添加
			head = emp;
		} else {
			Emp curent = head;
			while (true) {
				if (curent.next == null) {  //找到最后一个节点
					curent.next = emp;
					break;
				}
				//后移
				curent = curent.next;
			}
		}
	}

	//显示所有员工
	public void list(int i) {
		if (head == null) {
			System.out.println("第"+i+"个链表为空!");
		} else {
			Emp current = head;
			System.out.println("第"+i+"个链表的内容是:");
			while (true) {
				System.out.println("id=" + current.id + ",name=" + current.name);
				if (current.next == null) {
					break;
				}
				current = current.next;
			}
		}
	}

	//根据id查找员工
	public Emp findById(int id){
		Emp emp =null;
		if (head == null) {
			System.out.println("链表为空!");
		}else {
			Emp current = head;
			while (true) {
				if(current.id==id){
					emp= new Emp(id,current.name);
				}
				if (current.next == null) {
					break;
				}
				current = current.next;
			}
		}
		return emp;
	}
}
