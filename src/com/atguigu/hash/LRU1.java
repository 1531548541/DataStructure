package com.atguigu.hash;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 借助 LinkedHashMap
 */
public class LRU1<K,V> extends LinkedHashMap<K,V> {
    private int capacity;

    public LRU1(int capacity){
        super(capacity,0.75f,true);
        this.capacity=capacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return super.size()>capacity;
    }

    public static void main(String[] args) {
        LRU1 lru1=new LRU1(3);
        lru1.put(1,"a");
        lru1.put(2,"b");
        lru1.put(3,"c");
        System.out.println(lru1.keySet());
        lru1.get(2);
//        lru1.put(3,"c");
//        lru1.put(3,"c");
//        lru1.put(4,"x");
//        lru1.put(6,"x");
        System.out.println(lru1.keySet());
    }
}
