package com.atguigu.search;

import java.util.ArrayList;
import java.util.List;

/**
 * 注意：必须是有序的
 * 二分查找
 */
public class BinarySearch {

	public static void main(String[] args) {
		int[] arr = {1, 8, 10, 99, 888, 1000, 1000};
		System.out.println(binarySearch(arr, 0, arr.length - 1, 1000));
		System.out.println(binarySearch2(arr, 0, arr.length - 1, 1000));
	}

	/**
	 * 二分查找某一个数在数组中的index
	 *
	 * @param arr     给定的数组
	 * @param left    左索引
	 * @param right   右索引
	 * @param findVal 要找的数
	 * @return
	 */
	public static int binarySearch(int[] arr, int left, int right, int findVal) {
		int mid = (left + right) / 2;
		if (left > right) {
			return -1;
		}
		if (findVal > arr[mid]) {  //向右递归
			return binarySearch(arr, mid + 1, right, findVal);
		} else if (findVal < arr[mid]) {//向左递归
			return binarySearch(arr, left, mid - 1, findVal);
		} else {
			return mid;
		}
	}

	/**
	 * 二分查找某一个数在数组中的index集合
	 *
	 * @param arr     给定的数组
	 * @param left    左索引
	 * @param right   右索引
	 * @param findVal 要找的数
	 * @return
	 */
	public static List<Integer> binarySearch2(int[] arr, int left, int right, int findVal) {
		List<Integer> indexList = new ArrayList<>();
		int mid = (left + right) / 2;
		if (left > right) {
			return indexList;
		}
		if (findVal > arr[mid]) {  //向右递归
			return binarySearch2(arr, mid + 1, right, findVal);
		} else if (findVal < arr[mid]) {//向左递归
			return binarySearch2(arr, left, mid - 1, findVal);
		} else {
			/**
			 * 思路分析:
			 * 1.找到mid索引值，不要立即return
			 * 2.向mid索引值左边扫描，找到还满足的索引值
			 * 3，向mid索引值右边扫描，找到还满足的索引值
			 * 4.返回list
			 */
			indexList.add(mid);
			//向左找
			int temp = mid - 1;
			while (true) {
				if (temp < 0 || arr[temp] != findVal) {
					break;
				}
				indexList.add(temp);
				temp--;
			}
			//向右找
			temp = mid + 1;
			while (true) {
				if (temp > arr.length - 1 || arr[temp] != findVal) {
					break;
				}
				indexList.add(temp);
				temp++;
			}
			return indexList;
		}
	}
}
