package com.atguigu.search;

/**
 * 注意:必须是有序的
 * 插值查找
 */
public class InsertValueSearch {
	public static void main(String[] args) {
		int[] arr = {1, 8, 10, 99, 888, 1000, 1020};
		System.out.println(insertValueSearch(arr, 0, arr.length - 1, 1000));
	}


	private static int insertValueSearch(int[] arr, int left, int right, int findVal) {
		System.out.println("查找了~~~~");
		//自适应mid
		int mid = left + (right - left) * (findVal - arr[left]) / (arr[right] - arr[left]);
		if (left > right || findVal < arr[0] || findVal > arr[arr.length - 1]) {
			return -1;
		} else if (findVal > arr[mid]) {
			return insertValueSearch(arr, mid + 1, right, findVal);
		} else if (findVal < arr[mid]) {
			return insertValueSearch(arr, left, mid - 1, findVal);
		} else {
			return mid;
		}
	}
}
