package com.atguigu.search;

import java.util.Arrays;

/**
 * 斐波那契查找（黄金分割点）
 */
public class FibonacciSearch {

	public static int maxSize = 20;

	public static void main(String[] args) {
		int[] arr = {1, 8, 10, 89, 1000, 1024};
		System.out.println(fibonacciSearch(arr,1024));
	}

	//生成斐波那契数列
	public static int[] fib() {
		int[] f = new int[maxSize];
		f[0] = 1;
		f[1] = 1;
		for (int i = 2; i < maxSize; i++) {
			f[i] = f[i - 1] + f[i - 2];
		}
		return f;
	}

	//斐波那契查找（黄金分割点）
	public static int fibonacciSearch(int[] arr, int findVal) {
		int left = 0;
		int right = arr.length - 1;
		int k = 0; //斐波那契分割数的下标
		int mid = 0;
		int[] f = fib();
		//获得斐波那契分割数的下标
		while (right > f[k] - 1) {
			k++;
		}
		//因为f[k]值可能超过arr的长度，所以构造一个数组(默认不足的地方用0填充)
		int[] temp = Arrays.copyOf(arr, f[k]);
		//{1,8,10,89,1000,1024,0,0,0} =>  {1,8,10,89,1000,1024,1024,1024,1024}
		for (int i = right + 1; i < temp.length; i++) {
			temp[i] = arr[right];
		}
		//找到key
		while (left <= right) {
			mid = left + f[k - 1] - 1;
			if (findVal < temp[mid]) {  //向左查找
				right = mid - 1;
				/**为什么是k--
				 * 说明：1.全部元素=前面元素+后面元素
				 * 2.f[k]=f[k-1]+f[k-2]
				 *   因为前面有f[k-1]个元素，所以可以继续拆分f[k-1]=f[k-2]+f[k-3]
				 *   即在f[k-1]的前面继续查找k--
				 *   即下次循环mid=f[k-1-1]-1
				 */
				k--;
			} else if (findVal > temp[mid]) {//向右查找
				left = mid + 1;
				/**为什么是k-=2
				 * 说明：1.全部元素=前面元素+后面元素
				 * 2.f[k]=f[k-1]+f[k-2]
				 *   因为后面我们有f[k-2]，所以可以继续拆分f[k-1]=f[k-3]+f[k-4]
				 *   即在f[k-2]的前面继续查找k-=2
				 *   即下次循环mid=f[k-1-2]-1
				 */
				k -= 2;
			}else {  //找到
				//需要确定，返回的是哪个下标
				if (mid <= right) {
					return mid;
				}else {
					return right;
				}
			}
		}
		return -1;
	}
}
