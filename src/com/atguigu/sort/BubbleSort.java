package com.atguigu.sort;

import java.util.Arrays;

/**
 * 冒泡排序(从小到大)
 */
public class BubbleSort {
	public static void main(String[] args){
	 	int[] arr = {3,-1,9,10,-2};
	 	int temp=0;
		for (int i = 1; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-i; j++) {
				//如果前面的数>后面的数就交换位置
				if(arr[j]>arr[j+1]){
					temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		System.out.println(Arrays.toString(arr));
	}
}
