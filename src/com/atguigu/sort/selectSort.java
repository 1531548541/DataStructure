package com.atguigu.sort;

import java.util.Arrays;

/**
 * 选择排序(从小到大)
 * 即找最小值法
 */
public class selectSort {
	public static void main(String[] args){
		int[] arr = {3,-1,90,10,-2};
		int minIndex;
		int min;
		for (int i = 0; i < arr.length-1; i++) {
			minIndex=i;
			min=arr[i];
			for (int j = i+1; j < arr.length; j++) {
				if(min>arr[j]){
					minIndex=j;
					min=arr[j];
				}
			}
			if (minIndex != i) {
				//交换位置
				arr[minIndex]=arr[i];
				arr[i]=min;
			}
		}
		System.out.println(Arrays.toString(arr));
	}
}
