package com.atguigu.sort;

import java.util.Arrays;

/**
 * 基数排序
 */
public class RadixSort {

	public static void main(String[] args) {
		int[] arr = {53, 3, 542, 748, 14, 214};
		radixSort(arr);
	}

	public static void radixSort(int[] arr) {
		//二维数组表示10个桶
		int[][] bucket = new int[10][arr.length];
		//用于存放每个桶中的有效数据个数
		int[] bucketElementCounts = new int[10];

		//计算最大值
		int max = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr[i];
			}
		}
		//计算最大值的位数
		int maxLength = String.valueOf(max).length();

		for (int i = 0,n=1; i < maxLength; i++,n*=10) {
			for (int j = 0; j < arr.length; j++) {
				//取出每个元素的个位数
				int digitOfElement = arr[j] /n % 10;
				//放入对应的桶
				bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[j];
				bucketElementCounts[digitOfElement]++;
			}
			//按照这个桶的顺序(一维数组的下标依次取出数据，放入原来的数组)
			int index = 0;
			for (int k = 0; k < bucketElementCounts.length; k++) {
				if (bucketElementCounts[k] != 0) {
					for (int l = 0; l < bucketElementCounts[k]; l++) {
						arr[index++] = bucket[k][l];
					}
				}
				bucketElementCounts[k]=0;
			}
		}
		System.out.println("排序后:" + Arrays.toString(arr));
	}
}
