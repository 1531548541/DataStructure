package com.atguigu.sort;

import java.util.Arrays;

/**
 * 插入排序（从小到大）
 */
public class InsertSort {
	public static void main(String[] args){
		//3
		// [-1, 3, 10, 90, 90]
		//2
		//[-1, 3, 10, 10, 90]
		//1
		//[-1, 3, 3, 10, 90]
		//0
		//[-1, -1, 3, 10, 90]
		//[-2, -1, 3, 10, 90]
		int[] arr = {3,-1,90,10,-2};
		int insertVal;
		int insertIndex;
		for (int i = 1; i < arr.length; i++) {
			insertVal=arr[i];
			insertIndex=i-1;
			while (insertIndex>=0&&insertVal<arr[insertIndex]){
				arr[insertIndex+1]=arr[insertIndex];
				insertIndex--;
			}
			arr[insertIndex+1]=insertVal;
		}
		System.out.println(Arrays.toString(arr));
	}
}
