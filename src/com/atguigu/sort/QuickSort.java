package com.atguigu.sort;

import java.util.Arrays;

/**
 * 快速排序
 */
public class QuickSort {
	public static void main(String[] args){
		int[] arr={-9,78,0,23,-567,70};
		quickSort(arr,0,arr.length-1);
		System.out.println("排序后的数组:" + Arrays.toString(arr));
	}

	public static void quickSort(int[] arr,int left,int right){
		int l=left;
		int r=right;
		int temp=0;
		int middleValue=arr[(left+right)/2];
		//将所有比中间值小的放左边，大的放右边
		while(l<r){
			//在中间值左边一直找，找到大于等于中间值的才退出
			while(arr[l]<middleValue){
				l++;
			}
			//在中间值右边一直找，找到小于等于中间值的才退出
			while(arr[r]>middleValue){
				r--;
			}
			//如果l>=r,说明左右两边已经找好了
			if (l >= r) {
				break;
			}
			//交换
			temp=arr[l];
			arr[l]=arr[r];
			arr[r]=temp;

			if(arr[l]==middleValue){
				r--;
			}
			if (arr[r] == middleValue) {
				l++;
			}
		}

		if(l==r){
			l++;
			r--;
		}
		//向左递归
		if (left < r) {
			quickSort(arr,left,r);
		}
		//向右递归
		if (right>l) {
			quickSort(arr,l,right);
		}
	}
}
