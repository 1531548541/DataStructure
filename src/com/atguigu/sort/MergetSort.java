package com.atguigu.sort;

import java.util.Arrays;

/**
 * 归并排序
 */
public class MergetSort {
	public static void main(String[] args) {
		int[] arr = {8, 4, 5, 7, 1, 3, 6, 2};
		int[] temp = new int[arr.length];
		mergeSort(arr, 0, arr.length - 1, temp);
		System.out.println("排序后:" + Arrays.toString(arr));
	}

	/**
	 * 分+合
	 *
	 * @param arr   需要排序的数组
	 * @param left  左索引
	 * @param right 右索引
	 * @param temp  临时数组
	 */
	public static void mergeSort(int[] arr, int left, int right, int[] temp) {
		if (left < right) {
			int mid = (left + right) / 2;
			//向左递归进行分解
			mergeSort(arr, left, mid, temp);
			//向右递归进行分解
			mergeSort(arr, mid + 1, right, temp);
			//合并
			merge(arr, left, mid, right, temp);
		}
	}

	/**
	 * 合并方法
	 *
	 * @param arr   需要排序的数组
	 * @param left  左索引
	 * @param mid   中间索引
	 * @param right 右索引
	 * @param temp  临时数组
	 */
	public static void merge(int[] arr, int left, int mid, int right, int[] temp) {
		int i = left;
		int j = mid + 1;
		int t = 0;

		//(一)
		//先把左右两边数据按照规则填充到temp中
		//直到左右两边的有序序列有一边处理完毕
		while (i <= mid && j <= right) {
			if (arr[i] > arr[j]) {   //左边大于右边，将右边填充到temp
				temp[t] = arr[j];
				j++;
				t++;
			} else {
				temp[t] = arr[i];
				i++;
				t++;
			}
		}

		//(二)
		//把有剩余一边的数据依次填充到temp
		while (i <= mid) {   //左边有剩余
			temp[t] = arr[i];
			i++;
			t++;
		}
		while (j <= right) {   //右边有剩余
			temp[t] = arr[j];
			j++;
			t++;
		}
		//(三)
		//将temp拷贝到arr
		t = 0;
		int tempLeft = left;
		while (tempLeft <= right) {
			arr[tempLeft] = temp[t];
			t++;
			tempLeft++;
		}
	}
}
