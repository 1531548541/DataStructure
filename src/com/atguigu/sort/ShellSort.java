package com.atguigu.sort;

import java.util.Arrays;

/**
 * 希尔排序
 */
public class ShellSort {
	public static void main(String[] args) {
		int[] arr = {8, 9, 1, 7, 2, 3, 5, 4, 6, 0};
		shellSort2(arr);
	}

	//交换法
	public static void shellSort(int[] arr) {
		int temp = 0;
		/**
		 for (int i = 0; i < 5; i++) {
		 for (int j = i+5; j < arr.length; j+=5) {
		 //如果大于就交换
		 if (arr[i] > arr[j]) {
		 temp = arr[i];
		 arr[i] = arr[j];
		 arr[j] = temp;
		 }
		 }
		 }
		 System.out.println("排序后的数组:" + Arrays.toString(arr));
		 for (int a = 0; a < 2; a++) {
		 for (int i = a; i < arr.length; i+=2) {
		 for (int j = i+2; j < arr.length; j+=2) {
		 //如果大于就交换
		 if (arr[i] > arr[j]) {
		 temp = arr[i];
		 arr[i] = arr[j];
		 arr[j] = temp;
		 }
		 System.out.println(Arrays.toString(arr));
		 }
		 }
		 }
		 System.out.println("排序后的数组:" + Arrays.toString(arr));
		 for (int i = 0; i < arr.length-1; i++) {
		 //如果大于就交换
		 if (arr[i] > arr[i+1]) {
		 temp = arr[i];
		 arr[i] = arr[i+1];
		 arr[i+1] = temp;
		 }
		 }
		 System.out.println("排序后的数组:" + Arrays.toString(arr));
		 **/

		/**
		 * 非正规思路(个人)
		 int gap = arr.length/2;
		 while (gap!=1){
		 for (int a = 0; a < gap; a++) {
		 gap = gap/2;
		 for (int i = a; i < arr.length; i+=gap) {
		 for (int j = i+2; j < arr.length; j+=gap) {
		 //如果大于就交换
		 if (arr[i] > arr[j]) {
		 temp = arr[i];
		 arr[i] = arr[j];
		 arr[j] = temp;
		 }
		 System.out.println(Arrays.toString(arr));
		 }
		 }
		 }
		 }
		 for (int i = 0; i < arr.length-1; i++) {
		 //如果大于就交换
		 if (arr[i] > arr[i+1]) {
		 temp = arr[i];
		 arr[i] = arr[i+1];
		 arr[i+1] = temp;
		 }
		 }
		 System.out.println("排序后的数组:" + Arrays.toString(arr));
		 **/

		/**
		 * 正规思路
		 */
		for (int gap = arr.length / 2; gap > 0; gap /= 2) {
			for (int i = gap; i < arr.length; i++) {
				for (int j = i - gap; j >= 0; j -= gap) {
					//如果大于就交换
					if (arr[j] > arr[j + gap]) {
						temp = arr[j + gap];
						arr[j + gap] = arr[j];
						arr[j] = temp;
					}
				}
			}
			System.out.println("排序后的数组:" + Arrays.toString(arr));
		}

	}

	//优化: 移位法
	public static void shellSort2(int[] arr) {
		for (int gap = arr.length / 2; gap > 0; gap /= 2) {
			for (int i = gap; i < arr.length; i++) {
				int j = i;
				int temp = arr[j];
				if (arr[j] < arr[j - gap]) {
					while (j - gap >= 0 && temp < arr[j - gap]) {
						//移动
						arr[j] = arr[j - gap];
						j -= gap;
					}
					arr[j] = temp;
				}
			}
		}
		System.out.println("排序后的数组:" + Arrays.toString(arr));
	}

}
