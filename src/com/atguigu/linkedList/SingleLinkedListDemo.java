package com.atguigu.linkedList;

/**
 * 单链表(有序)
 */
public class SingleLinkedListDemo {
	public static void main(String[] args){
		HeroNode hero1 = new HeroNode(1,"松江","及时雨");
		HeroNode hero2 = new HeroNode(2,"卢俊义","玉麒麟");
		HeroNode hero3 = new HeroNode(3,"林冲","豹子头");
		HeroNode hero4 = new HeroNode(4,"郭永庆","山鸡哥");
		SingleLinkedList singleLinkedList = new SingleLinkedList();
		singleLinkedList.addNode(hero1);
		singleLinkedList.addNode(hero3);
		singleLinkedList.addNode(hero4);
		singleLinkedList.addNodeByNo(hero2);
		singleLinkedList.show();
		singleLinkedList.updateNode(new HeroNode(4,"李白","诗仙"));
		System.out.println("修改后___________________");
		singleLinkedList.show();
		singleLinkedList.delNode(3);
		System.out.println("删除后___________________");
		singleLinkedList.show();
	}
}

//定义一个单链表
class SingleLinkedList{
	//先初始化一个头节点，头节点不要动，不放具体数据
	private HeroNode head = new HeroNode();

	//添加节点(不考虑顺序，直接在末尾add)
	public void addNode(HeroNode heroNode){
		//找到最后一个节点
		HeroNode temp = head;
		while (true){
			if(temp.next==null){
				//添加节点
				temp.next=heroNode;
				break;
			}
			temp=temp.next;
		}
	}

	//添加节点(考虑顺序)
	public void addNodeByNo(HeroNode heroNode){
		HeroNode cur = head.next;
		while (cur!=null){
			if(cur.no==heroNode.no){
				System.out.printf("该no为%d的节点已存在，无法添加!",cur.no);
				break;
			}
			//如果是最后一个节点，在末尾添加节点
			if(cur.next==null){
				cur.next=heroNode;
				break;
			}
			//找到插入节点的前一个节点
			if(cur.no<heroNode.no && cur.next.no>heroNode.no){
				heroNode.next=cur.next;
				cur.next=heroNode;
				break;
			}
			cur=cur.next;
		}
	}

	//修改节点
	public void updateNode(HeroNode newNode){
		HeroNode cur = head.next;
		while (cur!=null){
			//找到需要修改的节点
			if (cur.no == newNode.no) {
				cur.name=newNode.name;
				cur.nickname=newNode.nickname;
				break;
			}
			cur=cur.next;
		}
	}

	//删除节点
	public void delNode(int no){
		HeroNode cur = head.next;
		while (cur!=null){
			//找到需要删除的节点
			if (cur.next.no == no) {
				cur.next=cur.next.next;
				break;
			}
			cur=cur.next;
		}
	}

	//显示链表
	public void show(){
		//判断链表是否为空
		if (head.next == null) {
			System.out.println("链表为空!");
			return;
		}
		HeroNode temp = head;
		while (true){
			System.out.println(temp);
			if(temp.next==null){
				break;
			}
			temp=temp.next;
		}
	}
}

//定义节点
class HeroNode{
	public int no;
	public String name;
	public String nickname;
	public HeroNode next;

	public HeroNode(){

	}

	public HeroNode(int no,String name,String nickname){
		this.no=no;
		this.name=name;
		this.nickname=nickname;
	}

	@Override
	public String toString() {
		return "HeroNode{" +
				"no=" + no +
				", name='" + name + '\'' +
				", nickname='" + nickname + '\'' +
				'}';
	}
}
