package com.atguigu.linkedList;

import java.awt.*;

/**
 * 约瑟夫问题
 */
public class Josepfu {
	public static void main(String[] args){
		CircleSingleLinkedList circleSingleLinkedList = new CircleSingleLinkedList();
		circleSingleLinkedList.add(5);
		circleSingleLinkedList.show();
		circleSingleLinkedList.countBoy(1,2,5);
	}
}

//创建一个环形单向链表
class CircleSingleLinkedList{
	//创建一个头节点
	BoyNode first =  null;

	//添加节点
	public void add(int nums){
		if (nums < 1) {
			System.out.println("输入的值不正确!");
			return;
		}
		BoyNode cur = null;
		for(int i=1;i<=nums;i++){
			BoyNode boyNode = new BoyNode(i);
			//如果是第一个节点
			if(i==1){
				first=boyNode;
				first.next=first;  //构成环
				cur=first;
			}else {
				cur.next=boyNode;
				boyNode.next= first;
				cur=boyNode;
			}
		}
	}

	//展示
	public void show(){
		if (first == null) {
			System.out.println("链表为空!");
			return;
		}
		BoyNode cur = first;
		while (true) {
			System.out.printf("小孩的编号是%d\n",cur.no);
			if (cur.next == first) {
				break;
			}
			cur=cur.next;
		}
	}

	/**
	 * 小孩出圈
	 * @param startNo  从哪个小孩开始
	 * @param countNum  数几个数
	 * @param nums  一共多少个小孩
	 */
	public void countBoy(int startNo,int countNum, int nums){
		if (first == null || startNo < 1 || startNo > nums) {
			System.out.println("输入的参数有误!");
			return;
		}
		//创建一个辅助指针
		BoyNode helper = first;
		while (helper.next != first) {
			helper=helper.next;
		}
		//小孩报数前，先让first和helper移动 k-1次
		for(int i=0;i<startNo-1;i++){
			first=first.next;
			helper=helper.next;
		}
		while (helper != first) {
			for(int i=0;i<countNum-1;i++){
				first=first.next;
				helper=helper.next;
			}
			System.out.printf("小孩%d出圈\n",first.no);
			first=first.next;
			helper.next=first;
		}
		System.out.printf("最后留在圈中的小孩编号%d\n",first.no);
	}
}

//创建一个节点
class BoyNode{
	public int no;
	public BoyNode next;

	public BoyNode(int no){
		this.no=no;
	}
}