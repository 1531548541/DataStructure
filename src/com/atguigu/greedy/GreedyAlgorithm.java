package com.atguigu.greedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * 贪心算法 (选择广播电台)
 */
public class GreedyAlgorithm {

    public static void main(String[] args) {
        //创建广播电台
        HashMap<String, HashSet<String>> broadCasts = new HashMap<>();
        //将各个电台放入其中
        HashSet<String> hashSet1 = new HashSet<>();
        hashSet1.add("北京");
        hashSet1.add("上海");
        hashSet1.add("天津");
        HashSet<String> hashSet2 = new HashSet<>();
        hashSet2.add("广州");
        hashSet2.add("北京");
        hashSet2.add("深圳");
        HashSet<String> hashSet3 = new HashSet<>();
        hashSet3.add("成都");
        hashSet3.add("上海");
        hashSet3.add("杭州");
        HashSet<String> hashSet4 = new HashSet<>();
        hashSet4.add("上海");
        hashSet4.add("天津");
        HashSet<String> hashSet5 = new HashSet<>();
        hashSet5.add("杭州");
        hashSet5.add("大连");
        //加入到map
        broadCasts.put("k1", hashSet1);
        broadCasts.put("k2", hashSet2);
        broadCasts.put("k3", hashSet3);
        broadCasts.put("k4", hashSet4);
        broadCasts.put("k5", hashSet5);
        //存放所有的地区
        HashSet<String> allAreas = new HashSet<>();
        allAreas.add("北京");
        allAreas.add("上海");
        allAreas.add("天津");
        allAreas.add("广州");
        allAreas.add("深圳");
        allAreas.add("成都");
        allAreas.add("杭州");
        allAreas.add("大连");

        //存放选择的电台集合
        List<String> selects = new ArrayList<>();
        while (allAreas.size() != 0) {
            //保存在一次遍历过程中，能够覆盖最大未覆盖的地区对应的电台的key
            String maxKey = null;
            for (String key : broadCasts.keySet()) {
                //存放遍历过程中的电台覆盖的地区和当前还没有覆盖的地区的交集
                HashSet<String> tempSet = new HashSet<>();
                //当前这个key能覆盖的地区
                HashSet<String> areas = broadCasts.get(key);
                tempSet.addAll(areas);
                //求出tempSet和allAreas交集,交集赋值给tempSet
                tempSet.retainAll(allAreas);
                //如果交集比maxKey对应的地区多，替换maxKey
                if (tempSet.size() > 0
                        && (maxKey == null || tempSet.size() > broadCasts.get(maxKey).size())) {
                    maxKey=key;
                }
            }
            if (maxKey != null) {
                selects.add(maxKey);
                //将选择过的地区从allAreas中remove
                allAreas.removeAll(broadCasts.get(maxKey));
            }
        }

        System.out.println("结果:"+selects);
    }
}
