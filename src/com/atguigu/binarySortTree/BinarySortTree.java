package com.atguigu.binarySortTree;

/**
 * 二叉排序树
 */
public class BinarySortTree {
	public static void main(String[] args){
		int[] arr={7,3,10,12,5,1,9,2};
		BinaryTree binaryTree=new BinaryTree();
		for (int i = 0; i < arr.length; i++) {
			binaryTree.add(new Node(arr[i]));
		}
		System.out.println("中序遍历:");
		binaryTree.infixOrder();
		//测试删除叶子节点
		binaryTree.delNode(7);
		System.out.println("删除后:");
		binaryTree.infixOrder();
	}
}

class BinaryTree{
	Node root;

	//添加节点
	public void add(Node node){
		if (root == null) {
			root=node;
		}else {
			root.add(node);
		}
	}

	//中序遍历
	public void infixOrder(){
		if (root != null) {
			root.infixOrder();
		}else {
			System.out.println("该二叉树为空!");
		}
	}

	//查找要删除的节点
	public Node search(int value){
		if (root == null) {
			return null;
		}else {
			return root.search(value);
		}
	}

	//查找要删除的父节点
	public Node searchParentNode(int value){
		if (root == null) {
			return null;
		}else {
			return root.searchParentNode(value);
		}
	}

	/**
	 * 删除node为根节点下的最小节点，并返回其值
	 * @param node
	 * @return
	 */
	public int delRightTreeMin(Node node){
		Node temp=node;
		while (temp.leftNode != null) {
			temp=temp.leftNode;
		}
		delNode(temp.value);
		return temp.value;
	}

	//删除节点
	public void delNode(int value){
		if (root == null) {
			return;
		}else {
			//查找到要删除的节点
			Node targetNode = search(value);
			if (targetNode == null) {
				return;
			}
			//如果当前二叉树只有一个节点(根节点)
			if (root.leftNode == null && root.rightNode == null) {
				root=null;
				return;
			}
			//找到要删除节点的父节点
			Node parentNode = searchParentNode(value);
			/**
			 * 如果要删除的是叶子节点
			 */
			if (targetNode.leftNode == null && targetNode.rightNode == null) {
				//判断是父节点的左节点还是右节点
				if (parentNode.leftNode == targetNode) {
					parentNode.leftNode=null;
				}else if(parentNode.rightNode==targetNode){
					parentNode.rightNode=null;
				}
			}
			/**
			 * 如果要删除的节点有左右子节点
			 */
			else if (targetNode.leftNode != null && targetNode.rightNode != null) {
				int minValue = delRightTreeMin(targetNode.rightNode);
				targetNode.value=minValue;
			}
			/**
			 * 如果要删除的节点有一个子节点
			 */
			else {
				if (targetNode.leftNode != null) {   //有左节点
					if (parentNode.leftNode == targetNode) {
						parentNode.leftNode=targetNode.leftNode;
					}else {
						parentNode.rightNode=targetNode.leftNode;
					}
				}else {  //有右节点
					if (parentNode.leftNode == targetNode) {
						parentNode.leftNode=targetNode.rightNode;
					}else {
						parentNode.rightNode=targetNode.rightNode;
					}
				}
			}
		}
	}
}

class Node{
	int value;
	Node leftNode;
	Node rightNode;

	public Node(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Node{" +
				"value=" + value +
				'}';
	}

	//添加节点
	public void add(Node node){
		if (node == null) {
			return;
		}
		//判断传入节点的值与当前子树的根节点的值的关系
		if (node.value < this.value) {
			if (this.leftNode == null) {
				this.leftNode=node;
			}else {
				//递归向左添加
				this.leftNode.add(node);
			}
		}else {
			if(this.rightNode==null){
				this.rightNode=node;
			}else {
				//递归向右添加
				this.rightNode.add(node);
			}
		}
	}

	//中序遍历
	public void infixOrder(){
		if (this.leftNode != null) {
			this.leftNode.infixOrder();
		}
		System.out.println(this);
		if (this.rightNode != null) {
			this.rightNode.infixOrder();
		}
	}

	//查找要删除的节点
	public Node search(int value){
		if(value==this.value){
			return this;
		}else if(value<this.value){
			if (this.leftNode == null) {
				return null;
			}else {
				return this.leftNode.search(value);
			}
		}else {
			if (this.rightNode == null) {
				return null;
			}else {
				return this.rightNode.search(value);
			}
		}
	}

	//查找要删除的节点的父节点
	public Node searchParentNode(int value){
		if((this.leftNode!=null&&this.leftNode.value==value)
		||(this.rightNode!=null&&this.rightNode.value==value)){
			return this;
		}else {
			if (value < this.value && this.leftNode != null) {
				return this.leftNode.searchParentNode(value);
			} else if (value >= this.value && this.rightNode != null) {
				return this.rightNode.searchParentNode(value);
			}else {
				return null;
			}
		}
	}
}
