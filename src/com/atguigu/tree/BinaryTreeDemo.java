package com.atguigu.tree;

/**
 * 二叉树
 */
public class BinaryTreeDemo {
	public static void main(String[] args) {
		BinaryTree binaryTree =new BinaryTree();
		HeroNode root=new HeroNode(0,"王者");
		HeroNode node1=new HeroNode(1,"小王");
		HeroNode node2=new HeroNode(2,"小李");
		HeroNode node3=new HeroNode(3,"小黑");
		HeroNode node4=new HeroNode(4,"小白");
		root.left=node1;
		root.right=node2;
		node2.right=node3;
		binaryTree.root=root;
		System.out.println("前序遍历:");
		binaryTree.preOrder();
		System.out.println("中序遍历:");
		binaryTree.infixOrder();
		System.out.println("后序遍历:");
		binaryTree.postOrder();

		System.out.println("前序查找:");
		System.out.println(binaryTree.preOrderSearch(3));
		System.out.println("中序查找:");
		System.out.println(binaryTree.infixOrderSearch(3));
		System.out.println("后序查找:");
		System.out.println(binaryTree.postOrderSearch(3));


		binaryTree.delNode(3);
		System.out.println("删除后前序查找:");
		binaryTree.preOrder();
	}
}

//定义二叉树
class BinaryTree{
	public HeroNode root;

	//前序遍历
	public void preOrder(){
		if(root!=null){
			root.preOrder();
		}else {
			System.out.println("该二叉树为空!");
		}
	}

	//中序遍历
	public void infixOrder(){
		if(root!=null){
			root.infixOrder();
		}else {
			System.out.println("该二叉树为空!");
		}
	}

	//后序遍历
	public void postOrder(){
		if(root!=null){
			root.postOrder();
		}else {
			System.out.println("该二叉树为空!");
		}
	}

	//前序查找
	public HeroNode preOrderSearch(int no){
		if(root!=null){
			return root.preOrderSearch(no);
		}else {
			return null;
		}
	}

	//中序查找
	public HeroNode infixOrderSearch(int no){
		if(root!=null){
			return root.infixOrderSearch(no);
		}else {
			return null;
		}
	}

	//后序查找
	public HeroNode postOrderSearch(int no){
		if(root!=null){
			return root.postOrderSearch(no);
		}else {
			return null;
		}
	}

	//删除节点
	public void delNode(int no){
		if (root != null) {
			//如果要删除的是根节点
			if(root.no==no){
				root=null;
			}else {
				root.delNode(no);
			}
		}else {
			System.out.println("该二叉树为空!不能删除!");
		}
	}
}


//定义节点
class HeroNode {
	public int no;
	public String name;
	public HeroNode left;
	public HeroNode right;

	public HeroNode(int no,String name){
		this.no=no;
		this.name=name;
	}

	@Override
	public String toString() {
		return "HeroNode{" +
				"no=" + no +
				", name='" + name + '\'' +
				'}';
	}

	//前序遍历
	public void preOrder(){
		//先输出根节点
		System.out.println(this);
		//输出左节点
		if (this.left != null) {
			this.left.preOrder();
		}
		//输出右节点
		if (this.right != null) {
			this.right.preOrder();
		}
	}

	//中序遍历
	public void infixOrder(){
		//输出左节点
		if (this.left != null) {
			this.left.infixOrder();
		}
		//先输出根节点
		System.out.println(this);
		//输出右节点
		if (this.right != null) {
			this.right.infixOrder();
		}
	}

	//后序遍历
	public void postOrder(){
		//输出左节点
		if (this.left != null) {
			this.left.postOrder();
		}
		//输出右节点
		if (this.right != null) {
			this.right.postOrder();
		}
		//先输出根节点
		System.out.println(this);
	}

	//前序查找
	public HeroNode preOrderSearch(int no){
		HeroNode resNode=null;
		//比较是否根节点
		if (this.no == no) {
			return this;
		}
		if(this.left!=null){
			resNode = this.left.preOrderSearch(no);
		}
		if(resNode!=null){
			return resNode;
		}
		if(this.right!=null){
			resNode = this.right.preOrderSearch(no);
		}
		return resNode;
	}

	//中序查找
	public HeroNode infixOrderSearch(int no){
		HeroNode resNode=null;
		if(this.left!=null){
			resNode = this.left.infixOrderSearch(no);
		}
		if(resNode!=null){
			return resNode;
		}
		//比较是否根节点
		if (this.no == no) {
			return this;
		}
		if(this.right!=null){
			resNode = this.right.infixOrderSearch(no);
		}
		return resNode;
	}

	//前序查找
	public HeroNode postOrderSearch(int no){
		HeroNode resNode=null;
		if(this.left!=null){
			resNode = this.left.postOrderSearch(no);
		}
		if(resNode!=null){
			return resNode;
		}
		if(this.right!=null){
			resNode = this.right.postOrderSearch(no);
		}
		if(resNode!=null){
			return resNode;
		}
		//比较是否根节点
		if (this.no == no) {
			return this;
		}
		return null;
	}

	//删除节点
	public void delNode(int no){
		if(this.left!=null&&this.left.no==no){
			this.left=null;
			return;
		}
		if(this.right!=null&&this.right.no==no){
			this.right=null;
			return;
		}
		if (this.left != null) {
			this.left.delNode(no);
		}
		if (this.right != null) {
			this.right.delNode(no);
		}
	}
}


