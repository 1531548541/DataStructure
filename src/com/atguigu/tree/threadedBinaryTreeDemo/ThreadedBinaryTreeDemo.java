package com.atguigu.tree.threadedBinaryTreeDemo;

/**
 * 线索化二叉树
 */
public class ThreadedBinaryTreeDemo {
	public static void main(String[] args) {
		ThreadedBinaryTree threadedBinaryTree = new ThreadedBinaryTree();
		HeroNode root=new HeroNode(1,"tome");
		HeroNode node2=new HeroNode(3,"tom");
		HeroNode node3=new HeroNode(6,"miki");
		HeroNode node4=new HeroNode(8,"simi");
		HeroNode node5=new HeroNode(10,"tick");
		HeroNode node6=new HeroNode(14,"dim");

		root.left=node2;
		root.right=node3;
		node2.left=node4;
		node2.right=node5;
		node3.left=node6;

		threadedBinaryTree.root=root;
		//线索化
		threadedBinaryTree.threadedNodes();
		//查看10号节点
		System.out.println("10号节点的前驱节点:"+node5.left);
		System.out.println("10号节点的后继节点:"+node5.right);

		//中序遍历线索化二叉树
		threadedBinaryTree.threadedList();
	}
}

//定义线索化二叉树
class ThreadedBinaryTree {
	public HeroNode root;
	public HeroNode pre;

	public void threadedNodes(){
		this.threadedNodes(root);
	}

	/**
	 * 线索化二叉树
	 * @param node
	 */
	public void threadedNodes(HeroNode node) {
		if (node == null) {
			return;
		}
		//(一)线索化左子树
		threadedNodes(node.left);
		//(二)线索化当前节点
		//处理当前节点的前驱节点
		if (node.left == null) {
			node.left = pre;
			node.leftType = 1;
		}

		//处理当前节点的后继节点
		if (pre != null && pre.right == null) {
			pre.right = node;
			pre.rightType = 1;
		}
		pre=node;
		//(三)线索化右子树
		threadedNodes(node.right);
	}

	/**
	 * 中序遍历线索化二叉树
	 */
	public void threadedList(){
		HeroNode node =root;
		while (node != null) {
			while(node.leftType==0){
				node=node.left;
			}
			//打印当前节点
			System.out.println(node);
			while (node.rightType == 1) {
				node=node.right;
				System.out.println(node);
			}
			//替换这个遍历的节点
			node=node.right;
		}
	}
}

//定义节点
class HeroNode {
	public int no;
	public String name;
	public HeroNode left;
	public HeroNode right;
	public int leftType;  //0表示左子树，1表示前驱节点
	public int rightType;  //0表示右子树，1表示后继节点

	public HeroNode(int no, String name) {
		this.no = no;
		this.name = name;
	}

	@Override
	public String toString() {
		return "HeroNode{" +
				"no=" + no +
				", name='" + name + '\'' +
				'}';
	}

	//前序遍历
	public void preOrder() {
		//先输出根节点
		System.out.println(this);
		//输出左节点
		if (this.left != null) {
			this.left.preOrder();
		}
		//输出右节点
		if (this.right != null) {
			this.right.preOrder();
		}
	}

	//中序遍历
	public void infixOrder() {
		//输出左节点
		if (this.left != null) {
			this.left.infixOrder();
		}
		//先输出根节点
		System.out.println(this);
		//输出右节点
		if (this.right != null) {
			this.right.infixOrder();
		}
	}

	//后序遍历
	public void postOrder() {
		//输出左节点
		if (this.left != null) {
			this.left.postOrder();
		}
		//输出右节点
		if (this.right != null) {
			this.right.postOrder();
		}
		//先输出根节点
		System.out.println(this);
	}

	//前序查找
	public HeroNode preOrderSearch(int no) {
		HeroNode resNode = null;
		//比较是否根节点
		if (this.no == no) {
			return this;
		}
		if (this.left != null) {
			resNode = this.left.preOrderSearch(no);
		}
		if (resNode != null) {
			return resNode;
		}
		if (this.right != null) {
			resNode = this.right.preOrderSearch(no);
		}
		return resNode;
	}

	//中序查找
	public HeroNode infixOrderSearch(int no) {
		HeroNode resNode = null;
		if (this.left != null) {
			resNode = this.left.infixOrderSearch(no);
		}
		if (resNode != null) {
			return resNode;
		}
		//比较是否根节点
		if (this.no == no) {
			return this;
		}
		if (this.right != null) {
			resNode = this.right.infixOrderSearch(no);
		}
		return resNode;
	}

	//前序查找
	public HeroNode postOrderSearch(int no) {
		HeroNode resNode = null;
		if (this.left != null) {
			resNode = this.left.postOrderSearch(no);
		}
		if (resNode != null) {
			return resNode;
		}
		if (this.right != null) {
			resNode = this.right.postOrderSearch(no);
		}
		if (resNode != null) {
			return resNode;
		}
		//比较是否根节点
		if (this.no == no) {
			return this;
		}
		return null;
	}

	//删除节点
	public void delNode(int no) {
		if (this.left != null && this.left.no == no) {
			this.left = null;
			return;
		}
		if (this.right != null && this.right.no == no) {
			this.right = null;
			return;
		}
		if (this.left != null) {
			this.left.delNode(no);
		}
		if (this.right != null) {
			this.right.delNode(no);
		}
	}
}
