package com.atguigu.tree;

/**
 * 数组存储完全二叉树
 */
public class ArrBinaryTreeDemo {
	public static void main(String[] args) {
		int[] arr = {1, 2, 3, 4, 5, 6, 7};
		ArrBinaryTree arrBinaryTree = new ArrBinaryTree(arr);
		arrBinaryTree.preOrder();  //1 2 4 5 3 6 7
	}
}

class ArrBinaryTree {

	public int[] arr;

	public ArrBinaryTree(int[] arr) {
		this.arr = arr;
	}

	public void preOrder() {
		this.preOrder(0);
	}

	/**
	 * 顺序存储二叉树的前序遍历
	 *
	 * @param index 数组下标
	 */
	public void preOrder(int index) {
		if (arr == null || arr.length == 0) {
			System.out.println("数组为空，无法进行前序遍历!");
		}
		//输出当前元素
		System.out.println(arr[index]);
		//向左递归
		if ((index * 2 + 1) < arr.length) {
			preOrder(index * 2 + 1);
		}
		//向右递归
		if ((index * 2 + 2) < arr.length) {
			preOrder(index * 2 + 2);
		}
	}
}