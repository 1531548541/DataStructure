package com.atguigu.tree;

import java.util.Arrays;

/**
 * 堆排序
 * 大顶堆(升序)
 * 小顶堆(降序)
 */
public class HeapSort {
	public static void main(String[] args) {
		//要求，升序
		int[] arr = {4, 6, 8, 5, 9};
		heapSort(arr);
	}

	public static void heapSort(int[] arr) {
		int temp = 0;
		System.out.println("堆排序...");
//		//分步完成
//		adjustHeap(arr,1,arr.length);
//		System.out.println("第一次"+ Arrays.toString(arr));
//		adjustHeap(arr,0,arr.length);
//		System.out.println("第2次"+ Arrays.toString(arr));

		//最终代码
		//1.将无序序列构建成一个堆，根据升序降序需求选择大顶堆或小顶堆
		for (int i = arr.length / 2 - 1; i >= 0; i--) {
			adjustHeap(arr, i, arr.length);
		}
		//2.将堆顶元素与末尾元素交换，将最大元素放到数组末端
		//3.重新调整结构，使其满足堆定义，然后继续交换堆顶元素与当前末尾元素，反复执行调整+交换步骤
		for (int j = arr.length - 1; j > 0; j--) {
			//交换
			temp = arr[j];
			arr[j] = arr[0];
			arr[0] = temp;
			adjustHeap(arr, 0, j);
		}
		System.out.println("堆排序后" + Arrays.toString(arr));
	}

	/**
	 * 将一个数组(二叉树)调整成一个大顶堆
	 * 举例: arr{4,6,8,5,9}  => i=1 => adjustHeap =>得到{4,9,8,5,6}
	 * 如果我们再次调整adjustHeap，传入的是i=0 => {4,9,8,5,6} => {9,6,8,5,4}
	 *
	 * @param arr    待排序的数组
	 * @param i      表示叶子节点在数组中的索引
	 * @param length 表示对多少个元素进行调整，length不断减小
	 */
	public static void adjustHeap(int arr[], int i, int length) {
		int temp = arr[i];   //先取出当前元素，保存起来
		//开始调整   2*i+1表示i节点的左子节点
		for (int j = 2 * i + 1; j < length; j = 2 * j + 1) {
			//左子节点小于右子节点
			if (j + 1 < length && arr[j] < arr[j + 1]) {
				j++;
			}
			//如果子节点大于父节点
			if (arr[j] > temp) {
				arr[i] = arr[j];  //把较大的值赋给当前接地那
				i = j;    //i指向j，继续循环比较
			} else {
				break;
			}
		}
		//当for循环结果时，我们已经将以i为父节点的树的最大值放在了最顶端
		arr[i] = temp;
	}
}

