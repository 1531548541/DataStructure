package com.atguigu.queue;

import java.util.Scanner;

/**
 *  环形数组队列
 */
public class CircleArrayQueueDemo {
	public static void main(String[] args){
		//演示过程
		CircleArrayQueue queue = new CircleArrayQueue(4);  //有效值最多是3个
		char key = ' ';//接受用户输入
		Scanner scanner = new Scanner(System.in);
		boolean loop = true;
		while (loop) {
			System.out.println("s(show):显示队列");
			System.out.println("e(exit):退出队列");
			System.out.println("a(add):入队");
			System.out.println("g(get):出队");
			System.out.println("h(head):显示队列头");
			key = scanner.next().charAt(0);
			switch (key){
				case 's':
					queue.showQueue();
					break;
				case 'e':
					scanner.close();
					loop = false;
					break;
				case 'a':
					System.out.println("请输入一个数:");
					int value = scanner.nextInt();
					queue.addQueue(value);
					break;
				case 'g':
					try {
						System.out.println("出队的是:"+queue.getQueue());
					}catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 'h':
					try {
						int result = queue.showHead();
						System.out.printf("队列头的数据是%d\n",result);
					}catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
			}
		}
		System.out.println("您已退出!");
	}
}

class CircleArrayQueue{
	private int maxSize;
	// front指向第一个元素:即 arr[front]
	//front初始值为0
	private int front;
	//rear 指向队列最后一个元素的后一个，因为要空出一个预留位置
	//rear初始值为0
	private int rear;
	private int[] arr; //存放数据的数组

	public CircleArrayQueue(int maxSize){
		this.maxSize=maxSize;
		arr = new int[this.maxSize];
	}

	//判断队列是否为空
	public boolean isNull(){
		return front==rear;
	}

	//判断队列是否满了
	public boolean isFull(){
		//下面两种方法都可以
// 		return (rear + 1)%maxSize==front;
		return getSize()==maxSize-1;
	}

	//入列
	public void addQueue(int num){
		if(isFull()){
			System.out.println("队列已满！");
			return;
		}
		arr[rear]=num;
		rear=(rear+1)%maxSize;
	}

	//出列
	public int getQueue(){
		if(isNull()){
			throw new RuntimeException("队列是空的!");
		}
		int value = arr[front];
		front=(front+1)%maxSize;
		return value;
	}

	//显示队列中所有数据
	public void showQueue(){
		if(isNull()){
			System.out.println("队列是空的!");
			return;
		}
		for (int i = front;i< front+getSize();i++) {
			System.out.printf("arr[%d]=",i%maxSize);
			System.out.println(arr[i%maxSize]);
		}
	}

	//显示队列的头数据（注意不是出列）
	public int showHead(){
		if(isNull()){
			throw new RuntimeException("队列是空的!");
		}
		return arr[front];
	}

	//获得数组中的有效值的个数
	public int getSize(){
		return (rear + maxSize - front)%maxSize;
	}
}
