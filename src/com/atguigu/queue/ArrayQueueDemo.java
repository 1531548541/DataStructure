package com.atguigu.queue;

import java.util.Scanner;

/**
 * 模拟队列
 * 存在问题；数组只能使用一次，即使全都出队任会满
 */
public class ArrayQueueDemo {
	public static void main(String[] args){
		//演示过程
		ArrayQueue queue = new ArrayQueue(3);
		char key = ' ';//接受用户输入
		Scanner scanner = new Scanner(System.in);
		boolean loop = true;
		while (loop) {
			System.out.println("s(show):显示队列");
			System.out.println("e(exit):退出队列");
			System.out.println("a(add):入队");
			System.out.println("g(get):出队");
			System.out.println("h(head):显示队列头");
			key = scanner.next().charAt(0);
			switch (key){
				case 's':
					queue.showQueue();
					break;
				case 'e':
					scanner.close();
					loop = false;
					break;
				case 'a':
					System.out.println("请输入一个数:");
					int value = scanner.nextInt();
					queue.addQueue(value);
					break;
				case 'g':
					try {
						System.out.println("出队的是:"+queue.getQueue());
					}catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
				case 'h':
					try {
						int result = queue.showHead();
						System.out.printf("队列头的数据是%d\n",result);
					}catch (Exception e){
						System.out.println(e.getMessage());
					}
					break;
			}
		}
		System.out.println("您已退出!");
	}
}

class ArrayQueue{
	private int maxSize;
	private int front; //队列头
	private int rear;  //队列尾
	private int[] arr; //存放数据的数组

	public ArrayQueue(int maxSize){
		this.maxSize=maxSize;
		front=rear=-1;  //默认头尾都是-1
		arr = new int[this.maxSize];
	}

	//判断队列是否为空
	public boolean isNull(){
		return front==rear;
	}

	//判断队列是否满了
	public boolean isFull(){
		return rear==maxSize-1;
	}

	//入列
	public void addQueue(int num){
		if(isFull()){
			System.out.println("队列已满！");
			return;
		}
		rear++;
		arr[rear]=num;
	}

	//出列
	public int getQueue(){
		if(isNull()){
			throw new RuntimeException("队列是空的!");
		}
		front++;
		return arr[front];
	}

	//显示队列中所有数据
	public void showQueue(){
		if(isNull()){
			System.out.println("队列是空的!");
			return;
		}
		for (int i = 0;i< arr.length;i++) {
			System.out.printf("arr[%d]=",i);
			System.out.println(arr[i]);
		}
	}

	//显示队列的头数据（注意不是出列）
	public int showHead(){
		if(isNull()){
			throw new RuntimeException("队列是空的!");
		}
		return arr[front+1];
	}
}
