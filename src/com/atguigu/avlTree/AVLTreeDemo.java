package com.atguigu.avlTree;

public class AVLTreeDemo {
	public static void main(String[] args) {
		int[] arr = {10, 11, 7, 6, 8, 9};  //需要旋转两次
		AVLTree binaryTree = new AVLTree();
		for (int i = 0; i < arr.length; i++) {
			binaryTree.add(new Node(arr[i]));
		}
		System.out.println("中序遍历:");
		binaryTree.infixOrder();
		System.out.println("平衡后---------");
		System.out.println("二叉树的高度="+binaryTree.root.height());
		System.out.println("左子节点高度="+binaryTree.root.leftHeight());
		System.out.println("右子节点高度="+binaryTree.root.rightHeight());
		System.out.println(binaryTree.root);
		System.out.println(binaryTree.root.rightNode);
	}
}

class AVLTree {
	Node root;

	//添加节点
	public void add(Node node) {
		if (root == null) {
			root = node;
		} else {
			root.add(node);
		}
	}

	//中序遍历
	public void infixOrder() {
		if (root != null) {
			root.infixOrder();
		} else {
			System.out.println("该二叉树为空!");
		}
	}

	//查找要删除的节点
	public Node search(int value) {
		if (root == null) {
			return null;
		} else {
			return root.search(value);
		}
	}

	//查找要删除的父节点
	public Node searchParentNode(int value) {
		if (root == null) {
			return null;
		} else {
			return root.searchParentNode(value);
		}
	}

	/**
	 * 删除node为根节点下的最小节点，并返回其值
	 *
	 * @param node
	 * @return
	 */
	public int delRightTreeMin(Node node) {
		Node temp = node;
		while (temp.leftNode != null) {
			temp = temp.leftNode;
		}
		delNode(temp.value);
		return temp.value;
	}

	//删除节点
	public void delNode(int value) {
		if (root == null) {
			return;
		} else {
			//查找到要删除的节点
			Node targetNode = search(value);
			if (targetNode == null) {
				return;
			}
			//如果当前二叉树只有一个节点(根节点)
			if (root.leftNode == null && root.rightNode == null) {
				root = null;
				return;
			}
			//找到要删除节点的父节点
			Node parentNode = searchParentNode(value);
			/**
			 * 如果要删除的是叶子节点
			 */
			if (targetNode.leftNode == null && targetNode.rightNode == null) {
				//判断是父节点的左节点还是右节点
				if (parentNode.leftNode == targetNode) {
					parentNode.leftNode = null;
				} else if (parentNode.rightNode == targetNode) {
					parentNode.rightNode = null;
				}
			}
			/**
			 * 如果要删除的节点有左右子节点
			 */
			else if (targetNode.leftNode != null && targetNode.rightNode != null) {
				int minValue = delRightTreeMin(targetNode.rightNode);
				targetNode.value = minValue;
			}
			/**
			 * 如果要删除的节点有一个子节点
			 */
			else {
				if (targetNode.leftNode != null) {   //有左节点
					if (parentNode.leftNode == targetNode) {
						parentNode.leftNode = targetNode.leftNode;
					} else {
						parentNode.rightNode = targetNode.leftNode;
					}
				} else {  //有右节点
					if (parentNode.leftNode == targetNode) {
						parentNode.leftNode = targetNode.rightNode;
					} else {
						parentNode.rightNode = targetNode.rightNode;
					}
				}
			}
		}
	}
}

class Node {
	int value;
	Node leftNode;
	Node rightNode;

	public Node(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Node{" +
				"value=" + value +
				'}';
	}

	//返回以该节点为根节点树的高度
	public int height() {
		return Math.max(leftNode == null ? 0 : leftNode.height(), rightNode == null ? 0 : rightNode.height()) + 1;
	}

	//返回左子树的高度
	public int leftHeight() {
		if (leftNode == null) {
			return 0;
		}
		return leftNode.height();
	}

	//返回右子树的高度
	public int rightHeight() {
		if (rightNode == null) {
			return 0;
		}
		return rightNode.height();
	}

	//左旋转
	public void leftRotate(){
		//以当前根节点的值创建新的节点
		Node newNode=new Node(this.value);
		//把新节点的左子树设置为当前节点的左子树
		newNode.leftNode=this.leftNode;
		//把新节点的右子树设置为当前节点的右子树的左子树
		newNode.rightNode=this.rightNode.leftNode;
		//把当前节点的值替换成右子节点的值
		this.value=this.rightNode.value;
		//把当前节点的右子数设置为当前节点右子数的右子数
		this.rightNode=this.rightNode.rightNode;
		//把当前节点的左子树设置成新节点
		this.leftNode=newNode;
	}

	//右旋转
	public void rightRotate(){
		Node newNode=new Node(this.value);
		newNode.rightNode=this.rightNode;
		newNode.leftNode=this.leftNode.rightNode;
		this.value=this.leftNode.value;
		this.leftNode=this.leftNode.leftNode;
		this.rightNode=newNode;
	}

	//添加节点
	public void add(Node node) {
		if (node == null) {
			return;
		}
		//判断传入节点的值与当前子树的根节点的值的关系
		if (node.value < this.value) {
			if (this.leftNode == null) {
				this.leftNode = node;
			} else {
				//递归向左添加
				this.leftNode.add(node);
			}
		} else {
			if (this.rightNode == null) {
				this.rightNode = node;
			} else {
				//递归向右添加
				this.rightNode.add(node);
			}
		}

		//当添加完一个节点后，如果 （右子数的高度-左子树的高度）>1 ,左旋转
		if (rightHeight()-leftHeight()>1){
			//如果它的右子树的左子树的高度大于它的右子数的右子数高度
			if (this.rightNode != null && this.rightNode.leftHeight() > this.rightNode.rightHeight()) {
				//先对右子数进行右旋转
				this.rightNode.rightRotate();
			}
			leftRotate();
			return;
		}
		//当添加完一个节点后，如果 （左子数的高度-右子树的高度）>1 ,右旋转
		if (leftHeight()-rightHeight()>1){
			//如果它的左子树的右子数高度大于它的左子树的左子树高度
			if (this.leftNode != null && this.leftNode.rightHeight() > this.leftNode.leftHeight()) {
				//先对当前节点的左子树进行左旋转
				this.leftNode.leftRotate();
			}
			rightRotate();
		}
	}

	//中序遍历
	public void infixOrder() {
		if (this.leftNode != null) {
			this.leftNode.infixOrder();
		}
		System.out.println(this);
		if (this.rightNode != null) {
			this.rightNode.infixOrder();
		}
	}

	//查找要删除的节点
	public Node search(int value) {
		if (value == this.value) {
			return this;
		} else if (value < this.value) {
			if (this.leftNode == null) {
				return null;
			} else {
				return this.leftNode.search(value);
			}
		} else {
			if (this.rightNode == null) {
				return null;
			} else {
				return this.rightNode.search(value);
			}
		}
	}

	//查找要删除的节点的父节点
	public Node searchParentNode(int value) {
		if ((this.leftNode != null && this.leftNode.value == value)
				|| (this.rightNode != null && this.rightNode.value == value)) {
			return this;
		} else {
			if (value < this.value && this.leftNode != null) {
				return this.leftNode.searchParentNode(value);
			} else if (value >= this.value && this.rightNode != null) {
				return this.rightNode.searchParentNode(value);
			} else {
				return null;
			}
		}
	}
}
