package com.atguigu.recursion;

/**
 * 八皇后问题
 */
public class Queue8 {

	int max = 8;
	static int count = 0;
	int[] array = new int[max];  //用于保存放置结果

	public static void main(String[] args) {
		Queue8 queue8 = new Queue8();
		queue8.check(0);
		System.out.printf("共有%d种方法", count);
	}

	//放置皇后
	public void check(int n) {
		if (n == max) {
			print();
			return;
		}
		//依次放入皇后，判断是否冲突
		for (int i = 0; i < array.length; i++) {
			//先把这个皇后n放到该行第一列
			array[n] = i;
			if (isConflict(n)) {  //如果不冲突
				check(n + 1);  //接着放下一个
			}
		}
	}

	//放置第n个皇后，检测是否与其他皇后冲突(在同一行列，斜线即为冲突)
	public boolean isConflict(int n) {
		for (int i = 0; i < n; i++) {
			if (array[i] == array[n] || Math.abs(n - i) == Math.abs(array[n] - array[i])) {
				return false;
			}

		}
		return true;
	}

	//打印拜访位置
	public void print() {
		count++;
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

}
