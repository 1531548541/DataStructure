package com.atguigu.huffmanTree;


import java.io.*;
import java.util.*;

/**
 * 哈夫曼编码
 */
public class HuffmanCode {
	public static void main(String[] args){
		String content="i like like like java do you like a java";
		byte[] contentBytes=content.getBytes();
//		System.out.println(contentBytes.length);
//		List<Node1> nodes = getNodes(contentBytes);
//		System.out.println("node="+nodes);
//		//创建哈夫曼树
//		Node1 huffmanTree = createHuffmanTree(nodes);
//		huffmanTree.preOrder();
//
//		//测试生成哈夫曼编码
//		getCodes(huffmanTree);
//		System.out.println("生成的哈夫曼编码表:"+huffmanCodes);
//
//		//测试zip
//		byte[] huffmanCodeBytes = zip(contentBytes, huffmanCodes);

		byte[] huffmanCodeBytes = huffmanZip(contentBytes);
		System.out.println("huffmanCodeBytes="+Arrays.toString(huffmanCodeBytes));
		byte[] decodeCode = decode(huffmanCodes, huffmanCodeBytes);
		System.out.println("解码后:"+new String(decodeCode));


		//测试压缩和解压文件
		String srcFile="d://hello.jpg";
		String dstFile="d://hello.zip";
		zipFile(srcFile,dstFile);
		System.out.println("压缩完成******");
		unZipFile(dstFile,"d://hello2.jpg");
	}

	/**
	 * 将main方法中的zip各步骤整合到这个方法中
	 * @param contentBytes
	 * @return
	 */
	private static byte[] huffmanZip(byte[] contentBytes){
		List<Node1> nodes = getNodes(contentBytes);
		//创建哈夫曼树
		Node1 huffmanTree = createHuffmanTree(nodes);
		//测试生成哈夫曼编码
		Map<Byte, String> huffmanCodes = getCodes(huffmanTree);
		//测试zip
		byte[] huffmanCodeBytes = zip(contentBytes, huffmanCodes);
		return huffmanCodeBytes;
	}

	private static List<Node1> getNodes(byte[] bytes){
		List<Node1> nodes=new ArrayList<>();
		//统计每个byte出现的次数
		Map<Byte,Integer> counts=new HashMap<>();
		for (Byte b : bytes) {
			Integer count=counts.get(b);
			if(count==null){
				counts.put(b,1);
			}else {
				counts.put(b,count+1);
			}
		}

		for (Map.Entry<Byte,Integer> entry : counts.entrySet()) {
			nodes.add(new Node1(entry.getKey(),entry.getValue()));
		}
		return nodes;
	}

	//通过list返回哈夫曼树
	private static Node1 createHuffmanTree(List<Node1> nodes){
		while (nodes.size() > 1) {
			//从小到大排序
			Collections.sort(nodes);
			//取出1和2节点
			Node1 leftNode=nodes.get(0);
			Node1 rightNode=nodes.get(1);
			Node1 parent=new Node1(null,leftNode.weight+rightNode.weight);
			parent.leftNode=leftNode;
			parent.rightNode=rightNode;
			//将1和2节点移除
			nodes.remove(leftNode);
			nodes.remove(rightNode);
			//将新的二叉树加入到nodes
			nodes.add(parent);
		}
		return nodes.get(0);
	}

	//为了调用方便，重载getCodes
	private static Map<Byte,String> getCodes(Node1 root){
		if(root==null){
			return null;
		}
		//处理root的左子树
		getCodes(root.leftNode,"0",stringBuilder);
		//处理root的右子树
		getCodes(root.rightNode,"1",stringBuilder);
		return huffmanCodes;
	}

	//生成哈夫曼树对应的哈夫曼编码
	//思路：
	//1. 将哈夫曼编码表存放在Map中，形式为: 32->01 97->100等
	static Map<Byte,String> huffmanCodes=new HashMap<>();
	//2. 在生成哈夫曼编码表示，需要去拼接路径，定义一个Stringbuilder存储某个叶子节点路径
	static StringBuilder stringBuilder=new StringBuilder();

	/**
	 *  将传入的node的所有叶子节点的哈夫曼编码得到，并放入huffmanCodes中
	 * @param node   传入节点
	 * @param code   路径:0表示左子节点  1表示右子节点
	 * @param stringBuilder  用于拼接路径
	 */
	private static void getCodes(Node1 node,String code,StringBuilder stringBuilder){
		StringBuilder stringBuilder1=new StringBuilder(stringBuilder);
		stringBuilder1.append(code);
		if (node != null) {
			//当前node是非叶子节点
			if(node.data==null){
				//向左递归
				getCodes(node.leftNode,"0",stringBuilder1);
				//向右递归
				getCodes(node.rightNode,"1",stringBuilder1);
			}else{//当前node是叶子节点
				huffmanCodes.put(node.data,stringBuilder1.toString());
			}
		}
	}

	private static byte[] zip(byte[] bytes,Map<Byte,String> huffmanCodes){
		//将bytes转成哈夫曼编码对应的字符串
		StringBuilder stringBuilder=new StringBuilder();
		for (byte b : bytes) {
			stringBuilder.append(huffmanCodes.get(b)); //stringBuilder= "101010001011111111110..."
		}
		/**
		 * 将"101010001011111111110..."转成byte[]
		 */
		//计算huffmanCodeBytes长度
		//方法一： int len=(stringBuilder.length()+7/8)
		int len;
		if(stringBuilder.length()%8==0){
			len=stringBuilder.length()/8;
		}else {
			len=stringBuilder.length()/8 +1;
		}
		byte[] huffmanCodeBytes=new byte[len];
		int index=0;
		for (int i = 0; i < stringBuilder.length(); i+=8) {
			String strByte;
			if(i+8>stringBuilder.length()){ //不够8位
				strByte=stringBuilder.substring(i);
			}else {
				strByte=stringBuilder.substring(i,i+8);
			}
			huffmanCodeBytes[index++]= (byte) Integer.parseInt(strByte,2);
		}
		return huffmanCodeBytes;
	}

	/**
	 * 将一个byte 转成一个二进制字符串
	 * @param flag  是否需要补高位
	 * @param b
	 * @return  返回b对应的二进制字符串 (注意是按补码返回)
	 */
	private static String byteToBitString(boolean flag,byte b){
		int temp=b;
		if (flag) {
			temp|=256;
		}
		String str=Integer.toBinaryString(temp);
		if (flag) {
			return str.substring(str.length()-8);
		}else {
			return str;
		}
	}

	/**
	 * 解码
	 * @param huffmanCodes
	 * @param huffmanBytes
	 * @return
	 */
	private static byte[] decode(Map<Byte,String> huffmanCodes,byte[] huffmanBytes){
		StringBuilder stringBuilder=new StringBuilder();
		for (int i = 0; i < huffmanBytes.length; i++) {
			//判断是否是最后一个字符
			boolean flag=!(i==huffmanBytes.length-1);
			stringBuilder.append(byteToBitString(flag,huffmanBytes[i]));
		}
//		System.out.println("哈夫曼字节数组对应的二进制字符串："+stringBuilder.toString());
		//将哈夫曼编码表进行调换，因为反向查询
		Map<String,Byte> map=new HashMap<>();
		for(Map.Entry<Byte, String> entry:huffmanCodes.entrySet()){
			map.put(entry.getValue(),entry.getKey());
		}

		List<Byte> list=new ArrayList<>();
		for (int i = 0; i < stringBuilder.length(); ) {
			int count=1;  //计数器
			boolean flag=true;
			Byte b=null;
			while (flag) {
				//101010001011111111110
				String key=stringBuilder.substring(i,i+count);
				b=map.get(key);
				if (b == null) {
					count++;
				}else {
					flag=false;  //跳出循环
				}
			}
			list.add(b);
			i+=count;
		}
		//当for循环结束后，list中存放了"i like like like java do you like a java"
		//将list放入byte[]
		byte[] b=new byte[list.size()];
		for (int i = 0; i < b.length; i++) {
			b[i]=list.get(i);
		}
		return b;
	}

	private static void zipFile(String srcFile,String dstFile){
		//创建输入流
		FileInputStream is=null;
		//创建输出流
		FileOutputStream os=null;
		ObjectOutputStream oos=null;
		try {
			is=new FileInputStream(srcFile);
			//创建一个和源文件大小一样的byte[]
			byte[] b=new byte[is.available()];
			//读取文件
			is.read(b);
			//对源文件进行压缩
			byte[] huffmanBytes = huffmanZip(b);
			//创建文件的输出流，存放压缩文件
			os=new FileOutputStream(dstFile);
			//创建一个和文件输出流关联的ObjectInputStream
			oos=new ObjectOutputStream(os);
			//把哈夫曼编码后的字符数组写入压缩文件
			oos.writeObject(huffmanBytes);
			//这里我们以对象流的方式写入哈夫曼编码，是为了以后我们恢复源文件时使用
			//注意一定要把哈夫曼编码写入压缩文件
			oos.writeObject(huffmanCodes);

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				is.close();
				oos.close();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 解压
	 * @param zipFile  准备解压的文件
	 * @param dstFile  将文件解压到哪个路径
	 */
	private static void unZipFile(String zipFile,String dstFile){
		//定义文件输入流
		InputStream is=null;
		//定义对象输入流
		ObjectInputStream ois=null;
		//定义文件的输出流
		OutputStream os=null;
		try {
			is=new FileInputStream(zipFile);
			ois=new ObjectInputStream(is);
			//读取哈夫曼字节数组
			byte[] huffmanBytes = (byte[]) ois.readObject();
			//读取哈夫曼编码表
			Map<Byte,String> huffmanCodes = (Map<Byte, String>) ois.readObject();
			//解码
			byte[] bytes = decode(huffmanCodes, huffmanBytes);
			//将bytes写入目标文件
			os=new FileOutputStream(dstFile);
			os.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				os.close();
				ois.close();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}



class Node1 implements Comparable<Node1>{
	Byte data;  //存储数据本身，比如 'a'=>97
	int weight;  //权重
	Node1 leftNode;
	Node1 rightNode;
	public Node1(Byte data,int weight){
		this.data=data;
		this.weight=weight;
	}

	@Override
	public int compareTo(Node1 o) {
		return this.weight-o.weight;
	}

	@Override
	public String toString() {
		return "Node1{" +
				"data=" + data +
				", weight=" + weight +
				'}';
	}

	//前序遍历
	public void preOrder(){
		System.out.println(this);
		if (this.leftNode != null) {
			this.leftNode.preOrder();
		}
		if (this.rightNode != null) {
			this.rightNode.preOrder();
		}
	}
}
