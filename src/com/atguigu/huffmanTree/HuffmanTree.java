package com.atguigu.huffmanTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 哈夫曼树
 */
public class HuffmanTree {
	public static void main(String[] args){
		int[] arr={13,7,8,3,29,6,1};
		Node root=createHuffmanTree(arr);
		preOrder(root);
	}

	//前序遍历
	public static void preOrder(Node root){
		if(root!=null){
			root.preOrder();
		}else {
			System.out.println("该树为空!");
		}
	}

	//创建哈夫曼树
	public static Node createHuffmanTree(int[] arr){
		List<Node> nodes=new ArrayList<>();
		for (int value : arr) {
			nodes.add(new Node(value));
		}
		while (nodes.size()>1){
			//按从小到大排序
			Collections.sort(nodes);
			/**
			 * 转变为哈夫曼树
			 */
			//取出最小的两个节点
			Node leftNode=nodes.get(0);
			Node rightNode=nodes.get(1);
			//构建一个新的二叉树
			Node parent=new Node(leftNode.value+rightNode.value);
			parent.leftNode=leftNode;
			parent.rightNode=rightNode;
			//将原来的nodes中处理过的二叉树删除
			nodes.remove(leftNode);
			nodes.remove(rightNode);
			//将parent加入nodes中
			nodes.add(parent);
		}
		return nodes.get(0);
	}
}

class Node implements Comparable<Node>{
	int value;
	Node leftNode;
	Node rightNode;

	public Node (int value){
		this.value=value;
	}

	@Override
	public String toString() {
		return "Node{" +
				"value=" + value +
				'}';
	}

	//前序遍历
	public void preOrder(){
		System.out.println(this);
		if (this.leftNode != null) {
			this.leftNode.preOrder();
		}
		if (this.rightNode != null) {
			this.rightNode.preOrder();
		}
	}

	@Override
	public int compareTo(Node o) {
		return this.value-o.value;
	}
}
