package com.atguigu.horse;

import org.ietf.jgss.Oid;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 骑士周游问题
 */
public class HorseChessBoard {
    private static final int row = 6;
    private static final int col = 6;
    private static int[][] chessboard = new int[row][col];  // 棋盘
    private static boolean finished = false;    // 标识是否全部走完

    public static void main(String[] args) {

        traversalChessboard(0,0,1);

        for (int[] item : chessboard){
            System.out.println(Arrays.toString(item));
        }
    }

    /**
     * 从 (x,y) 开始执行马踏棋盘算法
     * @param x 当前点的行坐标
     * @param y 当前点的列坐标
     * @param step 当前的步数
     */
    public static void traversalChessboard(int x, int y, int step){

        chessboard[x][y] = step;    // 记录当前点为第几步到达的

        List<Point> nextPoints = next(new Point(x, y));    // 获取当前点的下一步移动可选项集合
        sort(nextPoints);   // 将下一步移动的可选项集合按照它们的再下一步的可选项个数升序排序，此处体现了贪心

        /* 只要下一步可移动的选项个数大于 0，就继续往下走 */
        while (nextPoints.size() > 0){
            Point nextPoint = nextPoints.remove(0); // 排序后，移动到的下一个点的下一步的可选项个数时最小的
            if (chessboard[nextPoint.x][nextPoint.y] == 0){
                traversalChessboard(nextPoint.x, nextPoint.y, step+1);
            }
        }

        /*
            因为中途可能会走错，所以会有一个回溯的过程
        */
        if (step < row*col && !finished){   // 如果还未走完
            chessboard[x][y] = 0;   // 回溯时，要把走错的棋盘坐标置 0
        }else{
            finished = true;    // 如果步数达到了最大步数，说明走完了，就把标志位置为 true
        }
    }


    /**
     * 获取当前点下一步可以走的点的集合
     * @param curPoint  当前点
     */
    public static List<Point> next(Point curPoint){
        List<Point> points = new ArrayList<>();    // 当前点下一步可选的点集合
        Point point = new Point();

        /* 针对 8 种情况的判断 */
        if ((point.x=curPoint.x-2)>=0 && (point.y=curPoint.y-1)>=0){    // 左 2 上 1
            points.add(new Point(point));
        }
        if ((point.x=curPoint.x-1)>=0 && (point.y=curPoint.y-2)>=0){    // 左 1 上 2
            points.add(new Point(point));
        }
        if ((point.x=curPoint.x+1)<col && (point.y=curPoint.y-2)>=0){   // 右 1 上 2
            points.add(new Point(point));
        }
        if ((point.x=curPoint.x+2)<col && (point.y=curPoint.y-1)>=0){
            points.add(new Point(point));
        }
        if ((point.x=curPoint.x-2)>=0 && (point.y=curPoint.y+1)<row){
            points.add(new Point(point));
        }
        if ((point.x=curPoint.x-1)>=0 && (point.y=curPoint.y+2)<row){
            points.add(new Point(point));
        }
        if ((point.x=curPoint.x+1)<col && (point.y=curPoint.y+2)<row){
            points.add(new Point(point));
        }
        if ((point.x=curPoint.x+2)<col && (point.y=curPoint.y+1)<row){
            points.add(new Point(point));
        }
        return points;
    }

    /**
     * 将集合中的 Point 对象根据其下一步可移动选项的个数升序排序
     * @param points Point 对象集合
     */
    public static void sort(List<Point> points){
        points.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return next(o1).size() - next(o2).size();
            }
        });
    }
}
